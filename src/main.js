import './assets/css/bootstrap.min.css'
import './assets/css/paper-bootstrap-wizard.css'
// import './assets/css/demo.css'
// import './assets/css/themify-icons.css'

// import './assets/js/jquery-2.2.4.min.js'
// import './assets/js/bootstrap.min.js'
// import './assets/js/jquery.bootstrap.wizard.js'
// import './assets/js/demo.js'

import Vue from 'vue'
import App from './App.vue'
import router from './router/index'
import VueClip from 'vue-clip'
import Dialog from 'v-dialogs'
import axios from 'axios'
import VueAxios from 'vue-axios'
import VueSweetalert2 from 'vue-sweetalert2';
import 'sweetalert2/dist/sweetalert2.min.css';
import VueCookies from 'vue-cookies'
import VueSession from 'vue-session'
import { VuejsDatatableFactory } from 'vuejs-datatable';
import vuetify from './plugins/vuetify';


Vue.use( VuejsDatatableFactory );
Vue.use(VueSession)
Vue.use(VueCookies)
Vue.use(VueSweetalert2);
Vue.use(Dialog, {})
Vue.use(VueClip)
Vue.use(VueAxios, axios)
// import LoadScript from 'vue-plugin-load-script';
// import './assets/css/bootstrap.min.css'
// import './assets/css/paper-bootstrap-wizard.css'
// import './assets/css/demo.css'
// import './assets/css/themify-icons.css'

// import './assets/js/jquery-2.2.4.min.js'
// import './assets/js/bootstrap.min.js'
// import './assets/js/jquery.bootstrap.wizard.js'
// import './assets/js/demo.js'
// import './assets/js/paper-bootstrap-wizard.js'
// import './assets/js/jquery.validate.min.js'

Vue.config.productionTip = false

Vue.filter('DateToThai', function (value) {
  var day = value.split('-')[0]
  var monthNames = ['มกราคม', 'กุมภาพันธ์', 'มีนาคม', 'เมษายน', 'พฤษภาคม', 'มิถุนายน',
    'กรกฎาคม', 'สิงหาคม', 'กันยายน', 'ตุลาคม', 'พฤศจิกายน', 'ธันวาคม'
  ]
  return day + ' ' + monthNames[value.split('-')[1]] + ' ' + value.split('-')[2]
})

Vue.filter('DateToThaiManageCard', function (value) {
  var day = value.split('-')[2]
  var monthNames = ['มกราคม', 'กุมภาพันธ์', 'มีนาคม', 'เมษายน', 'พฤษภาคม', 'มิถุนายน',
    'กรกฎาคม', 'สิงหาคม', 'กันยายน', 'ตุลาคม', 'พฤศจิกายน', 'ธันวาคม'
  ]
  return day + ' ' + monthNames[parseInt(value.split('-')[1]) - 1] + ' ' + (parseInt(value.split('-')[0]) + 543)
})

Vue.filter('DateDMY', function (value) {
  if (value) {
    var monthNames = ['มกราคม', 'กุมภาพันธ์', 'มีนาคม', 'เมษายน', 'พฤษภาคม', 'มิถุนายน',
      'กรกฎาคม', 'สิงหาคม', 'กันยายน', 'ตุลาคม', 'พฤศจิกายน', 'ธันวาคม'
    ]

    var date = new Date(value)
    var day = date.getDate()
    var monthIndex = date.getMonth()
    var year = date.getFullYear()

    return day + ' ' + monthNames[monthIndex] + ' ' + year
  } else {
    return '-'
  }
})

new Vue({
  router,
  vuetify,
  render: h => h(App)
}).$mount('#app')
