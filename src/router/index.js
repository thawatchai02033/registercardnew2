import Vue from 'vue'
import Router from 'vue-router'
import Home from '../components/Home'
import Card1 from '../components/Card1'
import Card2 from '../components/Card2'
import Card3 from '../components/Card3'
import Card4 from '../components/Card4'
import ManageCard from '../components/ManageCard'
import ManageCardHR from '../components/ManageCardHR'
import ManageCardPP3S from '../components/ManageCardPP3S'
// import CardDoctorExtra from '../components/CardDoctorExtra'
// import CardExtra from '../components/CardExtra'

Vue.use(Router)

export default new Router({
    routes: [
        {
            path: '/',
            name: 'index',
            component: Home
        },
        {
            path: '/card1',
            name: 'บัตรบุคลากร',
            component: Card1
        },
        {
            path: '/card2',
            name: 'บัตรญาติบุคลากร',
            component: Card2
        },
        {
            path: '/card3',
            name: 'บัตรนักศึกษา',
            component: Card3
        },
        {
            path: '/card4',
            name: 'บัตรร้านค้า',
            component: Card4
        },
        {
            path: '/ManageCard',
            name: 'จัดการข้อมูลบัตร',
            component: ManageCard
        },
        {
            path: '/ManageCardHR',
            name: 'จัดการข้อมูลบัตรHR',
            component: ManageCardHR
        },
        {
            path: '/ManageCardPP3S',
            name: 'จัดการข้อมูลบัตรอาคาร',
            component: ManageCardPP3S
        },
        // {
        //     path: '/CardDoctorExtra',
        //     name: 'CardDoctorExtra',
        //     component: CardDoctorExtra
        // },
        // {
        //     path: '/CardExtra',
        //     name: 'CardExtra',
        //     component: CardExtra
        // }
    ]
})
