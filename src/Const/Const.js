// const ConnectionHost = "http://localhost:8080/"

const ConnectionHost = process.env.NODE_ENV === 'development'? "/MedHr" : "https://medhr.medicine.psu.ac.th"
const ConnectionHostPerson = process.env.NODE_ENV === 'development'? "/MedPerson" : "https://personnel.medicine.psu.ac.th"
const ConnectionLine = process.env.NODE_ENV === 'development'? "/Line" : "https://notify-api.line.me"
export default {
    ConnectionHost: ConnectionHost,
    ConnectionHostPerson: ConnectionHostPerson,
    ConnectionLine: ConnectionLine
}
