module.exports = {
  devServer: {
    proxy: {
      '/MedHr': {
        target: 'https://medhr.medicine.psu.ac.th',
        ws: true,
        // pathRewrite: { "^/MedHr/": "/MedHr/" },
        pathRewrite: { "^/MedHr": "" },
        changeOrigin: true,
        logLevel: "info"
        // logLevel: 'info', // or 'debug'
      },
      '/MedPerson': {
        target: 'https://personnel.medicine.psu.ac.th',
        ws: true,
        // pathRewrite: { "^/MedHr/": "/MedHr/" },
        pathRewrite: { "^/MedPerson": "" },
        changeOrigin: true,
        logLevel: "info"
        // logLevel: 'info', // or 'debug'
      },
      '/Line': {
        target: 'https://notify-api.line.me',
        ws: true,
        // pathRewrite: { "^/Line/": "/Line/" },
        pathRewrite: { "^/Line": "" },
        changeOrigin: true,
        logLevel: "info"
      }
    }
      // 'https://medhr.medicine.psu.ac.th/'
  },
  publicPath: './',
  "transpileDependencies": [
    "vuetify"
  ]
}
